package cocina;

public class Cocina {
	
	private Hornilla[] hornillas;
	private Horno horno;
	
	public Cocina(int temperaturaMin, int temperaturaMax) {
		// TODO Auto-generated constructor stub
		hornillas = new Hornilla[4];
		for (int i = 0; i < hornillas.length; i++) {
			hornillas[i] = new Hornilla();
		}
		horno = new Horno(temperaturaMin, temperaturaMax);
	}
	
	public void encenderHorno() {
		horno.encender();
	}
	
	public void apagarHorno() {
		horno.apagar();
	}
	
	public void aumentarHorno(int temp) {
		horno.aumentarTemperatura(temp);
	}
	
	public void disminuirHorno(int temp) {
		horno.disminuirTemperatura(temp);
	}

	public void encenderHornilla(int cual) {
		if(cual >= 0 && cual < 4){
			hornillas[cual].encender();
		}
	}
	
	public void apagarHornilla(int cual) {
		if(cual >= 0 && cual < 4){
			hornillas[cual].apagar();
		}
	}
	
	public void aumentarHornilla(int cual) {
		if(cual >= 0 && cual < 4){
			hornillas[cual].aumentarTemperatura();
		}
	}
	
	public void disminuirHornilla(int cual) {
		if(cual >= 0 && cual < 4){
			hornillas[cual].bajarTemperatura();
		}
	}
	
	public void apagarTodo(){
		for(int i = 0; i < hornillas.length; i++) {
			hornillas[i].apagar();
		}
		horno.apagar();
	}
	
	public void actualizar(){
		horno.actualizar();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder build = new StringBuilder("Esta cocina tiene ");
		build.append(hornillas.length).append(" hornillas").append("\n");
		for (int i = 0; i < hornillas.length; i++) {
			build.append(i).append(" ").append(hornillas[i]).append("\n");
		}
		build.append(" y un Horno ").append(horno);
		return build.toString();
	}

}
