package cocina;

public class Hornilla {

	private boolean encendida = false;
	private TEMPERATURA myTemperatura;
	
	public enum TEMPERATURA {
		ZERO, BAJA, MEDIA, ALTA
	}
	
	
	/**Crea un Objeto Hornilla.
	 * Inicialmente estara apagada.
	 * Por lo tanto con una Temperatura ZERO.
	 */
	public Hornilla() {
		encendida = false;
		myTemperatura = TEMPERATURA.ZERO;
	}
	
	/**Enciende la Hornilla. Si no se encuentra encendida,
	 * de lo contario no hace nada.
	 * A temperatura BAJA.
	 * Imprime el estado.
	 */
	public void encender(){
		if(!encendida) {
			encendida = true;
			myTemperatura = TEMPERATURA.BAJA;
			System.out.println(this);
		}
		else {
			System.out.println("Esta Hornilla ya se encuentra encendida..");
		}
	}
	
	/**Apaga la hornilla. Si no se encuentra apagada,
	 * de lo contrario no hace nada.
	 * Imprime el estado.
	 */
	public void apagar() {
		if(encendida) {
			encendida = false;
			myTemperatura = TEMPERATURA.ZERO;
			System.out.println(this);
		}
		else {
			System.out.println("Hornilla apagada, deberias encenderla primero..");
		}
		
	}
	
	/**Aumenta la temperatura de la hornilla,
	 * Siempre y cuando se encuentre encendidad y no 
	 * este ya en su maxima temperatura.
	 */
	public void aumentarTemperatura() {
		if(!encendida) {
			System.out.println("Hey esta hornilla esta apagada.. ");
			return;
			}
		
		if(myTemperatura == TEMPERATURA.BAJA) {
			myTemperatura = TEMPERATURA.MEDIA;
			System.out.println(this);
		}
		else if (myTemperatura == TEMPERATURA.MEDIA) {
			myTemperatura = TEMPERATURA.ALTA;
			System.out.println(this);
		}
		else
			System.out.println("Lo siento ya no se puede aumentar la temperatura de esta hornilla.");
		
	}
	
	/**Disminuye la temperatura de la hornilla,
	 * Siempre y cuando se encuentre encendidad y no 
	 * este ya en su minima temperatura.
	 */
	public void bajarTemperatura() {
		if(!encendida) {
			System.out.println("Hey esta hornilla esta apagada.. ");
			return;
			}
		if(myTemperatura == TEMPERATURA.ALTA) {
			myTemperatura = TEMPERATURA.MEDIA;
			System.out.println(this);
		}
		else if (myTemperatura == TEMPERATURA.MEDIA) {
			myTemperatura = TEMPERATURA.BAJA;
			System.out.println(this);
		}
		else
			System.out.println("Lo siento ya no se puede disminuir la temperatura de esta hornilla.");
	}
	
	/**
	 * 
	 * @return TRUE si esta encendida, FALSE en caso contrario.
	 */
	public boolean encendida(){
		return encendida;
	}
	
	/**Imprime el estado de la Hornilla.
	 * Si esta encendida o apagada, ademas si esta encedida imprime la 
	 * Temperatura a la que se encuentra.
	 * 
	 * @return Una representacion en cadena(String) del objeto.
	 * 
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Esta hornilla esta " + (encendida ? "ENCENDIDA" : "APAGADA" )  + " a temperatura " 
				+ (myTemperatura == TEMPERATURA.BAJA ? "BAJA" : (myTemperatura == TEMPERATURA.MEDIA ? "MEDIA" : 
					(myTemperatura == TEMPERATURA.ALTA ? "ALTA" : "ZERO.")));
	}
	
}
