package cocina;

public class Horno {
	
	private boolean encendido;
	private boolean aumentandoTemperatura;
	private boolean disminuyendoTemperatura;
	private int minTemperatura;
	private int maxTemperatura;
	private int temperaturaActual = 0;
	private int temperaturaObjetivo = 0;
	
	/**Crea un Objeto Horno, con una temperatura minima, y una temperatura maxima.
	 * 
	 * @param temperaturaMin La temperatura minima del horno.
	 * @param temperaturaMax La temperatura maxima del horno.
	 */
	public Horno(int temperaturaMin, int temperaturaMax) {
		minTemperatura = temperaturaMin;
		maxTemperatura = temperaturaMax;
		encendido = false;
		aumentandoTemperatura = false;
	}
	
	/**Enciende el horno, y lo lleva a la tempertura minima.
	 * Si el horno ya se encuentra no hace nada.
	 * 
	 */
	public void encender() {
		if(!encendido) {
			encendido = true;
			aumentarTemperatura(minTemperatura);
		}
		System.out.println("Horno Encendido.");
	}
	
	/**Apaga el Horno, y lo lleva a la temperatura minima.
	 * Si ya esta apagado no hace nada.
	 */
	public void apagar() {
		if(encendido) {
			disminuirTemperatura(minTemperatura);
			encendido = false;
		}
		System.out.println("Horno Apagado.");
	}

	/**Aumenta la temperatura del horno, para ello primero debe estar encendido.
	 * La temperatura solicitada no debe ser mayor a su maxima temperatura.
	 * 
	 * @param temperatura La temperatura a la cual se pretende aumentar.
	 */
	public void aumentarTemperatura(int temperatura) {
		// TODO Auto-generated method stub
		if(!encendido) {
			System.out.println("El Horno no esta encendido.");
			return;
		}
		
		if(temperatura > maxTemperatura) {
			System.out.println("No se puede Aumentar la temperatura por encima de " + maxTemperatura + " grados.");
			return;
		}
		if(temperatura > temperaturaActual) {
			temperaturaObjetivo = temperatura;
			aumentandoTemperatura = true;
		}
	}
	
	/**Disminuye la temperatura del horno, para ello primero debe estar encendido.
	 * La temperatura solicitada no debe ser menor a su minima temperatura.
	 * 
	 * @param temperatura La temperatura a la cual se pretende disminuir.
	 */
	public void disminuirTemperatura(int temperatura) {
		// TODO Auto-generated method stub
		if(!encendido) {
			System.out.println("El Horno no esta encendido.");
			return;
		}
		
		if(temperatura < minTemperatura) {
			System.out.println("No se puede Disminuir la temperatura por debajo de " + minTemperatura + " grados.");
			return;
		}
		if(temperatura < temperaturaActual) {
			temperaturaObjetivo = temperatura;
			disminuyendoTemperatura = true;
		}
	}
	
	/**Actualiza el estado del Horno.
	 * Creado para simular un aumento o disminucion progresiva de la temperatura. 
	 */
	public void actualizar() {
		if(aumentandoTemperatura) {
			if(temperaturaObjetivo > temperaturaActual) {
				temperaturaActual++;
			}
		}
		if(disminuyendoTemperatura) {
			if(temperaturaObjetivo < temperaturaActual ) {
				temperaturaActual--;
			}
		}
		if(temperaturaObjetivo == temperaturaActual) {
			disminuyendoTemperatura = aumentandoTemperatura = false;
		}
		
	}
	
	/**Imprime el estado del Horno.
	 * Si esta encendido o apagado, ademas si esta encedida imprime la 
	 * Temperatura a la que se encuentra.
	 * 
	 * @return Una representacion en cadena(String) del objeto.
	 * 
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "El horno esta " + (encendido ? "ENCENDIDO" : "APAGADO") + " temperatura Actual " + temperaturaActual 
				 + (encendido ? (temperaturaObjetivo > temperaturaActual ? " DISMINUYENDO" : " AUMENTANDO") : "");
	}
	

}
