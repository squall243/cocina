package cocina;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main{
	
	private Scanner myScanner;
	private Cocina myCocina; 
	private boolean abandonar = false;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Main();
		
	}
	
	public Main() {
		// TODO Auto-generated constructor stub
		myScanner = new Scanner(System.in);
		
		InputStreamReader input = new InputStreamReader(System.in, Charset.defaultCharset());
		
		String caracter = "";

		int max = 0;
		int min = 0;
		imprimirMensaje("¿Listo para crear una cocina?");
		max = getInt("Dime la Temperatura maxima del Horno..");
		min = getInt("Dime la Temperatura minima del Horno..");
		myCocina = new Cocina(min, max);
		imprimirMensaje("Bien!!!. Tu nueva cocina ha sido creada \n" + myCocina);
		
		char[] cbuf = new char[2];
		
		ayuda();
		
		
		while(!abandonar){
			try {
				input.read(cbuf);
				if(cbuf[0] == 'x')
					abandonar = true;
				if(cbuf[0] == 'v')
					encenderHornilla();
				if(cbuf[0] == 'b')
					apagarHornilla();
				if(cbuf[0] == 'd')
					aumentaHornilla();
				if(cbuf[0] == 'f')
					disminuirHornilla();
				if(cbuf[0] == 'h')
					encenderHorno();
				if(cbuf[0] == 'j')
					apagarHorno();
				if(cbuf[0] == 'y')
					aumentaHorno();
				if(cbuf[0] == 'u')
					disminuirHorno();
				if(cbuf[0] == 'q')
					ayuda();
				if(cbuf[0] == 'c')
					imprimirCocina();
				myCocina.actualizar();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		imprimirMensaje("Cerrando la cocina");
		myCocina.apagarTodo();
	}

	private void imprimirCocina() {
		// TODO Auto-generated method stub
		System.out.println(myCocina);
	}

	private void ayuda() {
		imprimirMensaje("Presiona X para salir del programa.");
		imprimirMensaje("Presiona V para encender una hornilla.");
		imprimirMensaje("Presiona B para apagar una hornilla.");
		imprimirMensaje("Presiona D para aumentar temperatura de una hornilla.");
		imprimirMensaje("Presiona F para disminuir temperatura de una hornilla.");
		imprimirMensaje("Presiona H para encender el horo.");
		imprimirMensaje("Presiona J para apagar el horno.");
		imprimirMensaje("Presiona Y para aumentar temperatura del horno.");
		imprimirMensaje("Presiona U para disminuir temperatura del horno.");
		imprimirMensaje("Presiona Q para imprimir este mensaje.");
	}
	
	private void disminuirHorno() {
		// TODO Auto-generated method stub
		int cuanto = getInt("¿A Cuanto quieres disminuirle la temperatura?");
		
		myCocina.disminuirHorno(cuanto);
	}

	private void aumentaHorno() {
		// TODO Auto-generated method stub
		int cuanto = getInt("¿A Cuanto quieres aumentarle la temperatura?");
		
		myCocina.aumentarHorno(cuanto);
	}

	private void apagarHorno() {
		// TODO Auto-generated method stub
		myCocina.apagarHorno();
	}

	private void encenderHorno() {
		// TODO Auto-generated method stub
		myCocina.encenderHorno();
		
	}

	public void imprimirMensaje(String msg) {
		System.out.println(msg);
	}
	
	public int getInt(String msg) {
		
		System.out.println(msg + ":");
		
		int i = -1;
		
		try{
			i = myScanner.nextInt();
		}catch (NumberFormatException e) {
			// TODO: handle exception
			System.out.println("Numero Invalido, solo introduzca valores enteros, sin decimales. Tampoco introduzca letras.");
		}catch (InputMismatchException e) {
			// TODO: handle exception
		}
		
		return i;
	}
	
	public void apagarHornilla() {
		
		int cual = getInt("¿Cual Hornilla quieres apagar?: 0 - 3");
		
		if(check(cual)) {
			myCocina.apagarHornilla(cual);
		}
	}
	
	private boolean check(int cual) {
		// TODO Auto-generated method stub
		return cual > -1;
	}

	public void encenderHornilla() {
		
		int cual = getInt("¿Cual Hornilla quieres encender?: 0 - 3");
		
		if(check(cual)) {
			myCocina.encenderHornilla(cual);
		}
	}
	
	public void aumentaHornilla(){
		
		int cual = getInt("¿A Cual hornilla quieres aumentarle la temperatura?: 0 - 3");
		
		if(check(cual)) {
			myCocina.aumentarHornilla(cual);
		}
	}
	
	public void disminuirHornilla(){
		
		int cual = getInt("¿A Cual hornilla quieres disminuirle la temperatura? 0 - 3");
		
		if(check(cual)) {
			myCocina.disminuirHornilla(cual);
		}
		
	}

}
